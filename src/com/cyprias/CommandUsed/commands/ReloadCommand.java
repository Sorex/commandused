package com.cyprias.CommandUsed.commands;

import java.util.List;

import org.bukkit.command.CommandSender;

import com.cyprias.CommandUsed.Plugin;
import com.cyprias.CommandUsed.command.Command;
import com.cyprias.CommandUsed.command.CommandAccess;
import com.cyprias.CommandUsed.ChatUtils;

public class ReloadCommand implements Command {
	public void listCommands(CommandSender sender, List<String> list) {
		if (sender.hasPermission("cu.reload"))
			list.add("/%s reload - Reload the plugin.");
	}

	public boolean execute(final CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
		if (!Plugin.checkPermission(sender, "cu.reload"))
			return false;
		Plugin instance = Plugin.getInstance();
		
		instance.reloadConfig();

		ChatUtils.send(sender, "Plugin reloaded.");

		return true;
	}

	public CommandAccess getAccess() {
		return CommandAccess.BOTH;
	}

	public void getCommands(CommandSender sender, org.bukkit.command.Command cmd) {
		ChatUtils.sendCommandHelp(sender, "cu.reload", "/%s reload - Reload the plugin.", cmd);
	}

	public boolean hasValues() {
		return false;
	}
}


