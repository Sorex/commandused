package com.cyprias.CommandUsed.commands;

import java.util.List;

import org.bukkit.command.CommandSender;
import com.cyprias.CommandUsed.ChatUtils;
import com.cyprias.CommandUsed.Plugin;
import com.cyprias.CommandUsed.command.Command;
import com.cyprias.CommandUsed.command.CommandAccess;


public class VersionCommand implements Command {
	public void listCommands(CommandSender sender, List<String> list) {
		if (sender.hasPermission("cu.version"))
			list.add("/%s version - Get the plugin version.");
	}

	public boolean execute(final CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
		if (!Plugin.checkPermission(sender, "cu.version"))
			return false;

		ChatUtils.send(sender, "�7We're running version v�f" + Plugin.getInstance().getDescription().getVersion());

		return true;
	}

	public CommandAccess getAccess() {
		return CommandAccess.BOTH;
	}

	public void getCommands(CommandSender sender, org.bukkit.command.Command cmd) {
		ChatUtils.sendCommandHelp(sender, "cu.version", "/%s version - Get the plugin version.", cmd);
	}

	public boolean hasValues() {
		return false;
	}
}
