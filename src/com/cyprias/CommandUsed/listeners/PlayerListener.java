package com.cyprias.CommandUsed.listeners;

import java.util.List;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.cyprias.CommandUsed.ChatUtils;
import com.cyprias.CommandUsed.Logger;
import com.cyprias.CommandUsed.Plugin;
import com.cyprias.CommandUsed.configuration.Config;

public class PlayerListener implements Listener {


	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
		String msg = event.getMessage();
		commandUsed(event.getPlayer(), msg);
	}

	public static void commandUsed(String playerName, String message) {
		Player player = Plugin.getInstance().getServer().getPlayer(playerName);
		if (player != null && player.isOnline())
			commandUsed(player, message);
	}
	
	
	public static void commandUsed(Player player, String message) {
		ConfigurationSection cmdalerts = Config.getConfigurationSection("cmdalerts");
		Set<String> groupSection = cmdalerts.getKeys(false);
		List<String> commands;
		String worldName = player.getWorld().getName();
		
		for (String groupType : groupSection) {
			Set<String> worlds = cmdalerts.getConfigurationSection(groupType).getKeys(false);
			for (String world : worlds) {
				if (world.equalsIgnoreCase("global") || worldName.matches(world)){
					commands = cmdalerts.getConfigurationSection(groupType).getStringList(world);
					for (String regex : commands) {
						regex = regex.trim();
						message = message.trim();
						if (message.matches(regex)) {
							if (!player.hasPermission("cu.exemptalert."+groupType)){
								Logger.info(player.getName() + " used " + message + " at " + Plugin.getPlayerLoc(player));
								ChatUtils.permBroadcast("cu.cmdalert." + groupType,
									ChatColor.GRAY	+ "[" + ChatColor.YELLOW+ groupType + ChatColor.GRAY + "] " + Config.getString("messages.stCommandUsed", Plugin.getColouredName(player), message, Plugin.getPlayerLoc(player)));
							}
						}
					}
				}
			}
		}
	}
	
}
