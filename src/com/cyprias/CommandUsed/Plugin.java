package com.cyprias.CommandUsed;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Handler;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import com.cyprias.CommandUsed.command.CommandManager;
import com.cyprias.CommandUsed.commands.ReloadCommand;
import com.cyprias.CommandUsed.commands.VersionCommand;
import com.cyprias.CommandUsed.configuration.Config;
import com.cyprias.CommandUsed.configuration.YML;
import com.cyprias.CommandUsed.listeners.PlayerListener;

public class Plugin  extends JavaPlugin {
	private static Plugin instance = null;
	public static String chatPrefix = "[CU] ";
	
	Handler handler;
	public void onEnable() {
		instance = this;
		
		// Check if config.yml exists on disk, copy it over if not. This keeps
		// our comments intact.
		if (!(new File(getDataFolder(), "config.yml").exists())) {
			Logger.info("Copying config.yml to disk.");
			try {
				YML.toFile(getResource("config.yml"), getDataFolder(), "config.yml");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}

		// Check if the config on disk is missing any settings, tell console if
		// so.
		try {
			Config.checkForMissingProperties();
		} catch (IOException e4) {
			e4.printStackTrace();
		} catch (InvalidConfigurationException e4) {
			e4.printStackTrace();
		}
		
		// Register our event listenres.
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		
		// Regster our commands.
		CommandManager cm = new CommandManager();
		cm.registerCommand("version", new VersionCommand());
		cm.registerCommand("reload", new ReloadCommand());
			
		getCommand("cu").setExecutor(cm);
		
		// Grab text sent to the game console, so we can track powertool messages. 
		// Prevent users with powertool from avoiding the command used notification.
		handler = new LogHandler();
		olog.addHandler(handler);

		// Start plugin metrics, see how popular our plugin is.
		if (Config.getBoolean("properties.use-metrics")){
			try {
				new Metrics(this).start();
			} catch (IOException e) {}
		}
		
		Logger.info("Loaded!");
		
	}
	public void onDisable() {
		olog.removeHandler(handler);
	}

	static java.util.logging.Logger olog = java.util.logging.Logger.getLogger("Minecraft");
	
	public static final Plugin getInstance() {
		return instance;
	}


	public static String getPlayerLoc(Player player) {
		return getPlayerLoc(player.getLocation());
	}

	public static String getPlayerLoc(Location loc) {
		return loc.getWorld().getName() + "|" + loc.getBlockX() + " " + loc.getBlockZ();
	}
	
	
	public static String getColouredName(CommandSender sender){
		// Used to grab colour from mChat. 
		
		String senderName = sender.getName();
		if (sender instanceof Player) {
			Player player = (Player) sender;
		
			senderName = player.getDisplayName();
		}
		
		return senderName;
	}
	
	public static boolean checkPermission(CommandSender sender, String permission) {
		if (!sender.hasPermission(permission)) {
			ChatUtils.error(sender, String.format(Config.getString("messages.NoPermission"), permission));
			return false;
		}
		return true;
	}
}
