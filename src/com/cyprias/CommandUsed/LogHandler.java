package com.cyprias.CommandUsed;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

import com.cyprias.CommandUsed.listeners.PlayerListener;

public class LogHandler extends Handler {
	public LogHandler() {
		super();
	}

	public void publish(LogRecord record) {
		if (!isLoggable(record))
			return;
		
		String msg = record.getMessage();
		//Logger.info("format msg: " + msg);
		String regex = "\\[PT\\] (\\w*) issued server command: (.*)";
		if (msg.matches(regex)) {
			String player = msg.replaceFirst(regex, "$1");
			String command = msg.replaceFirst(regex, "$2");
			PlayerListener.commandUsed(player, command);
		}
		
		
	}

	public void flush() {}

	public void close() throws SecurityException {}
}